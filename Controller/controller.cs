/*
Classe abstrai o gerenciamento da entrada de um jogador.
Para adicionar um novo layout controle deve adicionar os devidos botões em 
Projeto->Configurações de projeto->Mapa de Entrada, e após isto adicionar
os novos botões em layouts.
*/

using Godot;
using System;
using System.Collections.Generic;

public partial class controller : Node {
	private enum Mappings {
		MoveUp,
		MoveDown,
		MoveLeft,
		MoveRight,
		PlaceBomb,
	}

	private static List<Dictionary<Mappings, string>> layouts = new List<Dictionary<Mappings, string>>{
		new Dictionary<Mappings, string> {
			{ Mappings.MoveUp, "p1_up" },
			{ Mappings.MoveDown, "p1_down" },
			{ Mappings.MoveLeft, "p1_left" },
			{ Mappings.MoveRight, "p1_right" },
			{ Mappings.PlaceBomb, "p1_bomb" },
		},
		new Dictionary<Mappings, string> {
			{ Mappings.MoveUp, "p2_up" },
			{ Mappings.MoveDown, "p2_down" },
			{ Mappings.MoveLeft, "p2_left" },
			{ Mappings.MoveRight, "p2_right" },
			{ Mappings.PlaceBomb, "p2_bomb" },
		},
	};

	[Export]
	public int Layout = 0;

	[Signal]
	public delegate void MoveUpEventHandler(bool pressed);
	[Signal]
	public delegate void MoveDownEventHandler(bool pressed);
	[Signal]
	public delegate void MoveLeftEventHandler(bool pressed);
	[Signal]
	public delegate void MoveRightEventHandler(bool pressed);

	[Signal]
	public delegate void PlaceBombEventHandler();


	private Dictionary<Mappings, Action<bool>> signals;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready() {
		signals = new Dictionary<Mappings, Action<bool>> {
			{Mappings.MoveUp, p => EmitSignal(SignalName.MoveUp, p)},
			{Mappings.MoveDown, p => EmitSignal(SignalName.MoveDown, p)},
			{Mappings.MoveLeft, p => EmitSignal(SignalName.MoveLeft, p)},
			{Mappings.MoveRight, p => EmitSignal(SignalName.MoveRight, p)},
			{Mappings.PlaceBomb, p => { if (p) EmitSignal(SignalName.PlaceBomb); }},
		};
	}

	public override void _Input(InputEvent @event) {
		foreach(var mapping in layouts[Layout]) {
			if (@event.IsActionPressed(mapping.Value)) {
				signals[mapping.Key].Invoke(true);
			}
			if (@event.IsActionReleased(mapping.Value)) {
				signals[mapping.Key].Invoke(false);
			}
		}
	}
}
