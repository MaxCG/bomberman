/*
Classe responsável por renderizar o visual do mapa no jogo. Este visual
inclui: Terrenos, Explosões e Contagem das explosões.
*/

using System.Linq;
using Godot;
using Godot.Collections;
using System.Collections;

public partial class MapRenderer : TileMap
{
	private Dictionary<Map.Tile, int> terrain = new Dictionary<Map.Tile, int>{
				{Map.Tile.Wall, 1},
				{Map.Tile.Obstacle, 2},
				{Map.Tile.Explosion, 3},
				{Map.Tile.Empty, 0},
				{Map.Tile.Hole, 4},
			};

	private Dictionary<string, int> explosionCount = new();

	public void SetTileType(Map.Tile type, Vector2I position, int layer = 0) {
		var col = new Array<Vector2I>();
		col.Add(position);
		SetCellsTerrainConnect(layer, col, terrain[type], 0);
	}

	public void SetTileType(Map.Tile type, Array<Vector2I> positions, int layer = 0) {
		SetCellsTerrainConnect(layer, positions, terrain[type], 0);
	}
	
	public int GetTileType(Vector2I position, int layer = 0){
		return GetCellTileData(layer, position, false).TerrainSet;
	}

	public void SetTemporaryTileType(Map.Tile type, Array<Vector2I> tiles, float timeSeconds, System.Action onTimeout, int layer = 0) {
		SetTileType(type, tiles, layer);
		var timer = new Timer();
		AddChild(timer);
		timer.OneShot = true;
		timer.WaitTime = timeSeconds;
		timer.Timeout += () => { 
			RemoveChild(timer); onTimeout(); 
		};
		timer.Start();
	}

	public void SetExplosion(Array<Vector2I> tiles, float timeSeconds, System.Action<Vector2I> ActionOnBox, System.Action<Vector2I> ActionAfterExplosion, int layer = 0) {
		var tileSet = tiles.ToHashSet();

		Array<Vector2I> explodedObstacles = new Array<Vector2I>();
		foreach (var tile in tileSet) {
			if (GetTileType(tile) == terrain[Map.Tile.Obstacle]) {
				explodedObstacles.Add(tile);
			}
		}
		SetTileType(Map.Tile.Empty, explodedObstacles);

		var OnExplosionEnd = () => {
			RemoveExplosion(tileSet, ActionAfterExplosion);
			foreach (var tile in explodedObstacles) {
				ActionOnBox(tile);
				GD.Print(tile);
			}
		};

		SetTemporaryTileType(Map.Tile.Explosion, new Array<Vector2I>(tileSet.ToArray()), timeSeconds, OnExplosionEnd, layer);
		AddExplosionCountToMap(tileSet);
	}

	private void AddExplosionCountToMap(System.Collections.Generic.ISet<Vector2I> tiles) {
		foreach (var tile in tiles) {
			var exists = explosionCount.TryGetValue(GetDictionaryKey(tile), out int value);
			if (exists) {
				explosionCount[GetDictionaryKey(tile)]++;
				continue;
			}
			
			explosionCount[GetDictionaryKey(tile)] = 1;
			
		}
	}

	private void RemoveExplosion(System.Collections.Generic.ISet<Vector2I> tiles, System.Action<Vector2I> ActionAfterExplosion) {
		foreach (var tile in tiles) {
			var count = explosionCount[GetDictionaryKey(tile)];
			if (count > 1) {
				count--;
				explosionCount[GetDictionaryKey(tile)] = count;
				continue;
			}

			explosionCount[GetDictionaryKey(tile)] = 0;
			SetTileType(Map.Tile.Empty, tile);
			ActionAfterExplosion(tile);
		}
	}

	private string GetDictionaryKey(Vector2I tile) {
		return tile.X.ToString() + "," + tile.Y.ToString() + ".";
	}
}
