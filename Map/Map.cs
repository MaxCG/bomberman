/*
Classe responsável por criar e gerenciar mapas no jogo. Ela implementa
leitura do arquivo de layout (Classe MapReader utilizada os layouts
da pasta Templates), Posicionamento dos jogadores, explosão das
bombas e IO dos jogadores.
*/

using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class MapReader {
	private string file;

	public MapReader(string mapFile) {
		file = mapFile;
	}

	public List<List<Map.Tile>> Load() {
		GD.Print(file);
		List<List<Map.Tile>> list;
		try {
			var template = FileAccess.GetFileAsString(file);

			var lines = template.Split('\n');

			var dimensions = lines[0].Split(' ');
			var dimX = Int32.Parse(dimensions[0]);
			var dimY = Int32.Parse(dimensions[1]);

			list = new List<List<Map.Tile>>(dimX + 2);

			Map.Tile tile;

			for (int i = -1; i < dimX + 1; i++) {
				list.Add(new List<Map.Tile>(dimY + 2));
				for (int j = -1; j < dimY + 1; j++) {
					if (i == -1 || j == -1 || i == dimX || j == dimY) {
						tile = Map.Tile.Wall;
					} else {
						switch (lines[j+1][i]) {
							case 'X':
								tile = Map.Tile.Wall;
								break;
							case '.':
								tile = Map.Tile.Empty;
								break;
							case 'O':
								tile = Map.Tile.Obstacle;
								break;
							case 'H':
								tile = Map.Tile.Hole;
								break;
							default:
								throw new Exception($"Invalid block at position [{i}][{j}]");
						}
					}

					list[i + 1].Add(tile);
				}
			}
		} catch (Exception e) {
			throw new Exception($"Could not read map template: {e}");
		}

		return list;
	}
}

public partial class Map : Node2D, IPowerUpGeneratorSetter {
	public enum Tile {
		Wall,
		Obstacle,
		Explosion,
		Empty,
		Hole,
	}
	
	private static int mapIndex = 0;

	private int numberOfPlayersAlive;

	[Export]
	private MapRenderer renderer;

	[Export]
	private PackedScene BombScene;

	private List<List<Map.Tile>> map;

	private PowerUpGenerator powerUpGenerator = new PowerUpGeneratorSequential();

	private PlayerFactory playerFactory;
	private List<Player> players;

	public static void ChangeMapTemplate(int index) {
		mapIndex = index;
	}

	public static bool canPutExplosion(Tile tile){
		return tile != Tile.Wall;
	}

	public void HandlePlayerDamage(Player player) {
		player.ReduceHealth();

		var timer = new Timer();
		if (player.HasHealth()) {
			AddChild(timer);
			timer.OneShot = true;
			timer.WaitTime = GlobalConfig.playerRespawnTime;
			timer.Timeout += () => {
				RespawnPlayer(player);
				RemoveChild(timer);
			};
			timer.Start();
		} else {
			numberOfPlayersAlive--;
			if (GameEnded()) {
				// for debugging
				GD.Print("Game Ended!");

				AddChild(timer);
				timer.WaitTime = GlobalConfig.gameOverWaitTime;
				timer.Timeout += () => {
					var menuPackedScene = GlobalConfig.GetPackedScene(GlobalConfig.Scenes.Menu);
					GetTree().ChangeSceneToPacked(menuPackedScene);
				};
				timer.Start();
			}
		}
	}
	
	private void PutPlayerOutsideMap(Player player){
		const float INF = 2000f;
		player.setPosition(new Vector2(INF,INF));
	}

	private void RespawnPlayer(Player player) {
		player.MoveAndSlide();
		PutPlayerOutsideMap(player);
		player.Respawn();
	}

	private void spawnPlayer(Vector2I position) {
		Player player = playerFactory.createPlayer(renderer.MapToLocal(position));
		if(player == null){
			GD.Print("Reached maximum number of Players");
			return;
		}
		AddChild(player);
		player.PlayerDied += HandlePlayerDamage;
		players.Add(player);
		numberOfPlayersAlive++;
	}
	
	private void setHubPlayers(Player player1, Player player2){
		HUB hub = GetNode<Control>("HUB") as HUB;
		hub.setPlayers(player1, player2);
	}
	
	private void setHubPowerUpGenerator(){
		HUB hub = GetNode<Control>("HUB") as HUB;
		hub.setPowerUpSetter(this);
	}

	private void placePlayers(){
		playerFactory = new PlayerFactory();
		Vector2I positionPlayer1 = new Vector2I(1,1);
		Vector2I positionPlayer2 = new Vector2I(map.Count-2, map[0].Count-2);
		players = new List<Player>();
		spawnPlayer(positionPlayer1);
		spawnPlayer(positionPlayer2);
		setHubPlayers(players[0],players[1]);
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready() {
		map = new MapReader(GlobalConfig.GetMapTemplate(mapIndex)).Load();
		Vector2I pos = new Vector2I();
		for (int i = 0; i < map.Count; i++) {
			pos.X = i;
			for (int j = 0; j < map[i].Count; j++) {
				pos.Y = j;
				renderer.SetTileType(map[i][j], pos);
			}
		}
		placePlayers();
		setHubPowerUpGenerator();
	}

	public void AckBomb(Bomb b) {
		b.Exploded += HandleBombExplosion;
		b.initiateBomb(players);
		AddChild(b);
	}

	private void HandleBombExplosion(Bomb b) {
		var nearestTile = renderer.LocalToMap(renderer.ToLocal(b.Position));

		var affectedBlocks = b.getBehavior().Expand((pos) => map[pos.X][pos.Y], nearestTile);

		Action<Vector2I> ActionOnBox = (pos) => {
			var powerUp = powerUpGenerator.generatePowerUp(renderer.MapToLocal(pos));
			AddChild(powerUp);
		};
		
		Action<Vector2I> ActionAfterExplosion = (pos) => {
			Map.Tile originalTile = map[pos.X][pos.Y];
			
			Map.Tile tileAfterExplosion = Map.Tile.Empty;
			if(originalTile == Tile.Hole){
				tileAfterExplosion = Tile.Hole;
			}
			renderer.SetTileType(tileAfterExplosion, pos);
		};

		renderer.SetExplosion(affectedBlocks, GlobalConfig.explosionDuration, ActionOnBox, ActionAfterExplosion);
	}

	public bool GameEnded() {
		return numberOfPlayersAlive <= 1;
	}
	
	public Vector2 getTilePosition(Vector2 position){
		return renderer.MapToLocal(renderer.LocalToMap(position));
	}
	
	
	
	public void setPowerUpGenerator(PowerUpGenerator powerUpGenerator_){
		powerUpGenerator = powerUpGenerator_;
	}
}
