#if TOOLS
using Godot;

[Tool]
public partial class Plugin : EditorPlugin {
	private string prefix = "res://Components/";

	public override void _EnterTree() {
		// Initialization of the plugin goes here.
		var script = GD.Load<Script>(prefix + "ChangeSceneButton.cs");
		AddCustomType("ChangeSceneButton", "Button", script, null);
	}

	public override void _ExitTree() {
		// Clean-up of the plugin goes here.
		RemoveCustomType("ChangeSceneButton");
	}
}
#endif
