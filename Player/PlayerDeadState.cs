/*
Classe destinada a definir como o estado do jogador fica ao morrer. 
A animação esta definida nos sprite.
*/

using Godot;
using System;

public class PlayerDeadState : PlayerState
{
	const string fireAnimationName = "Dead";
	const string fallAnimationName = "Fall";
	
	private string deadAnimationName;
	
	public PlayerDeadState(PlayerState.Event evt){
		if(evt == PlayerState.Event.DamageFire){
			deadAnimationName = fireAnimationName;
		}else{
			deadAnimationName = fallAnimationName;
		}
	}


	protected override void whenEnterState(Player player){
		player.EmitSignal(Player.SignalName.PlayerDied, player);
		player.playAnimation(deadAnimationName);
	}

	protected override void whenExitState(Player player){
		player.resetSpriteScale();
		return;
	}

	public override void processDirection(Player p, PlayerState.Direction d, bool pressed) {
	}

	public override void processEvent(Player p, PlayerState.Event evt) {
	}

	public override Vector2 movementVector() {
		return Vector2.Zero;
	}

	public override bool isDead() {
		return true;
	}
}
