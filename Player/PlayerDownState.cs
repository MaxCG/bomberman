/*
Classe destinada a definir como o estado do jogado deve ficar depois de
ter recebido o IO de i para baixo. A animação de movimento e animação
de espera estão definidas nos sprites.
*/

using Godot;
using System;

public class PlayerDownState : PlayerMovementState
{
	public PlayerDownState(){
		movementDirection = new Vector2(0,1);
		animationIdle = "IdleDown";
		animationMovement = "MoveDown";
		facingDirection = PlayerState.Direction.Down;
	}
}
