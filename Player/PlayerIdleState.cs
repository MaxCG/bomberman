/*
Classe destinada a definir como o estado do jogador ao inicio do jogo ou quando
o jogador renascer. 
*/

using Godot;
using System;

public class PlayerIdleState : PlayerRightState {
	public PlayerIdleState(): base() {
		moving = false;
	}

	public void setSprite(Player player) {
		whenEnterState(player);
	}
}
