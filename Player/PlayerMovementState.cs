/*
Classe abstrata que define como os estados onde o jogador está em movimento
se comportam. Ela não define os atributos, isto é responsabilidade de 
suas subclasses.
*/

using Godot;
using System;

public abstract class PlayerMovementState : PlayerState
{
	protected Vector2 movementDirection;
	protected string animationIdle;
	protected string animationMovement;
	protected PlayerState.Direction facingDirection;
	protected bool moving = true;

	private void startPlayingMovement(Player player){
		player.playAnimation(animationMovement);
	}

	private void startPlayingIdle(Player player){
		player.playAnimation(animationIdle);
	}

	protected override void whenEnterState(Player player){
		player.playAnimation(animationMovement);
	}

	protected override void whenExitState(Player player){

	}

	public override void processDirection(Player p, PlayerState.Direction d, bool pressed) {
		if (d == facingDirection) {
			this.moving = pressed;
			if (this.moving) {
				startPlayingMovement(p);
			} else {
				startPlayingIdle(p);
			}
			return;
		}

		if (pressed == false) return;

		switch (d) {
			case PlayerState.Direction.Up:
				changeState(p, new PlayerUpState());
				return;
			case PlayerState.Direction.Down:
				changeState(p, new PlayerDownState());
				return;
			case PlayerState.Direction.Left:
				changeState(p, new PlayerLeftState());
				return;
			case PlayerState.Direction.Right:
				changeState(p, new PlayerRightState());
				return;
		}

		throw new Exception();
	}

	public override void processEvent(Player p, PlayerState.Event evt) {
		changeState(p, new PlayerDeadState(evt));		
	}

	public override Vector2 movementVector() {
		if (moving) {
			return movementDirection;
		} else {
			return Vector2.Zero;
		}
	}

	public override bool isDead() {
		return false;
	}

}
