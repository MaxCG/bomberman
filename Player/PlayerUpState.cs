/*
Classe destinada a definir como o estado do jogado deve ficar depois de
ter recebido o IO de i para cima. A animação de movimento e animação
de espera estão definidas nos sprites.
*/

using Godot;
using System;

public class PlayerUpState : PlayerMovementState
{
	public PlayerUpState(){
		movementDirection = new Vector2(0,-1);
		animationIdle = "IdleUp";
		animationMovement = "MoveUp";
		facingDirection = PlayerState.Direction.Up;
	}
}
