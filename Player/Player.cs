/*
Descreve a implementação do Jogador no jogo. Nela está funções 
relacionadas ao comportamento de movimento, criação de bombas e 
interações com o mapa.
*/

using Godot;

public partial class Player : CharacterBody2D {
	private int health = GlobalConfig.initialHealth;
	private int speed = GlobalConfig.initialSpeed;

	[Signal]
	public delegate void PlayerDiedEventHandler(Player player);

	private IExplosionBehavior bombBehavior = new SimpleExplosionBehavior();

	PackedScene Bomb = GlobalConfig.GetPackedScene(GlobalConfig.Scenes.Bomb);
	Map MapReference;

	PlayerState currentState = new PlayerIdleState();
	public AnimationPlayer AnimationSprite;

	private int availableBombs = GlobalConfig.initialAvailableBombs;
	
	private Vector2 initialPosition;
	
	private void SetDamageCollision(bool value) {

		var detectFireArea2D = GetNode("DetectFire") as Area2D;
		detectFireArea2D.SetCollisionMaskValue(2, value);
		
		var detectFallArea2D = GetNode("DetectFall") as Area2D;
		detectFallArea2D.SetCollisionMaskValue(3, value);
	}
	
	public void setSpeed(int s){
		speed = s;
	}

	public int getSpeed(){
		return speed;
	}
	
	public int getHealth(){
		return health;
	}

	public void IncreaseAvailableBombs() {
		availableBombs++;
	}

	public void setExplosionBehavior(IExplosionBehavior explosionBehavior) {
		bombBehavior = explosionBehavior;
	}

	public void IncreaseExplosionRange(int increment) {
		bombBehavior.IncreaseExplosionRange(increment);
	}

	public void playAnimation(string animation){
		AnimationSprite.Play(animation);
	}

	private void initializeChildReferences(){
		AnimationSprite = GetNode<AnimationPlayer>("Sprite2D/AnimationSprite");
	}

	public bool isDead() {
		return currentState.isDead();
	}

	public bool HasHealth() {
		return (health > 0);
	}

	public void IncreaseHealth(int increment) {
		health += increment;
	}

	public void ReduceHealth() {
		health--;
	}

	public void Respawn() {
		currentState.resetState(this);
		SetDamageCollision(true);
		setPosition(initialPosition);
	}

	public void setSpriteReference(string spritePath){
		Sprite2D sprite = GetNode<Sprite2D>("Sprite2D");
		sprite.Texture = GD.Load<Texture2D>(spritePath);
	}
	
	public void resetSpriteScale(){
		Sprite2D sprite = GetNode<Sprite2D>("Sprite2D");
		var scale = GlobalConfig.initialSpriteScale;
		sprite.Scale = new Vector2(scale,scale);
	}

	public void setControllerLayout(int n){
		GetNode<controller>("Controller").Layout = n;
	}

	public void setState(PlayerState state) {
		currentState = state;
	}

	public override void _Ready() {
		initializeChildReferences();
		MapReference = GetParent() as Map;
	}

	private void createNewBomb() {
		var bomb = Bomb.Instantiate() as Bomb;
		bomb.Position = MapReference.getTilePosition(Position);
		bomb.setBehavior(bombBehavior.clone());
		bomb.bombOwner = this;
		MapReference.AckBomb(bomb);
	}

	private bool canPlaceBomb() {
		return !isDead() && availableBombs > 0;
	}

	private void placeBomb(){
		if(canPlaceBomb()){
			availableBombs--;
			createNewBomb();
		}
	}

	public void moveUp(bool p){
		movementHandler(PlayerState.Direction.Up, p);
	}
	public void moveDown(bool p){
		movementHandler(PlayerState.Direction.Down, p);
	}
	public void moveLeft(bool p){
		movementHandler(PlayerState.Direction.Left, p);
	}
	public void moveRight(bool p){
		movementHandler(PlayerState.Direction.Right, p);
	}

	private void movementHandler(PlayerState.Direction d, bool pressed){
		currentState.processDirection(this, d, pressed);
	}

	private void movePlayer(){
		Velocity = currentState.movementVector() * getSpeed();
		MoveAndSlide();
	}

	public void setPosition(Vector2 position){
		Position = position;
	}
	
	public void setInitialPosition(Vector2 position){
		initialPosition = position;
		setPosition(position);
	}

	private bool IsOnFire(){
		Area2D detectFireArea2D = GetNode("DetectFire") as Area2D;
		return detectFireArea2D.HasOverlappingBodies();
	}
	
	private bool IsOnHole(){
		Area2D detectFallArea2D = GetNode("DetectFall") as Area2D;
		return detectFallArea2D.HasOverlappingBodies();
	}

	private void takeDamageIfApplicable() {
		if (IsOnFire()) {
			currentState.processEvent(this, PlayerState.Event.DamageFire);
			SetDamageCollision(false);
		}
		else if (IsOnHole()) {
			currentState.processEvent(this, PlayerState.Event.DamageFall);
			SetDamageCollision(false);
		}
	}

	public override void _PhysicsProcess(double delta)
	{
		movePlayer();
		takeDamageIfApplicable();
	}

	public override void _Process(double delta){
		takeDamageIfApplicable();
	}
}
