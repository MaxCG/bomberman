/*
Classe destinada a definir como o estado do jogado deve ficar depois de
ter recebido o IO de i para esquerda. A animação de movimento e animação
de espera estão definidas nos sprites.
*/

using Godot;
using System;

public class PlayerLeftState : PlayerMovementState
{
	public PlayerLeftState(){
		movementDirection = new Vector2(-1,0);
		animationIdle = "IdleLeft";
		animationMovement = "MoveLeft";
		facingDirection = PlayerState.Direction.Left;
	}
}
