/*
Classe abstrata que define o contrato que um estado de jogador
deve respeitar.
*/

using Godot;
using System;

public abstract class PlayerState {
	public enum Direction {
		Up,
		Down,
		Left,
		Right
	}

	public enum Event {
		DamageFire,
		DamageFall,
	}

	protected abstract void whenEnterState(Player player);
	protected abstract void whenExitState(Player player);
	protected void changeState(Player player, PlayerState nextState) {
		whenExitState(player);
		player.setState(nextState);
		nextState.whenEnterState(player);
	}

	public abstract void processDirection(Player p, Direction d, bool pressed);
	public abstract void processEvent(Player p, Event evt);
	public abstract Vector2 movementVector();
	public abstract bool isDead();
	
	public void resetState(Player player){
		changeState(player, new PlayerIdleState());
	}
}
