/*
Classe destinada a definir como o estado do jogado deve ficar depois de
ter recebido o IO de i para direita. A animação de movimento e animação
de espera estão definidas nos sprites.
*/

using Godot;
using System;

public class PlayerRightState : PlayerMovementState
{
	public PlayerRightState(){
		movementDirection = new Vector2(1,0);
		animationIdle = "IdleRight";
		animationMovement = "MoveRight";
		facingDirection = PlayerState.Direction.Right;
	}
}
