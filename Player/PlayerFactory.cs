/*
Classe abstrai a criação de players para o jogo. Realizando as inicializações 
necessárias para a criação dos mesmo.
*/

using Godot;
using System;
using System.Collections.Generic;

public class PlayerFactory
{
	const string PURPLE_SPRITE_PATH = "res://Player/Sprites/PlayerTileSetInicial.png";
	const string ORANGE_SPRITE_PATH = "res://Player/Sprites/PlayerTileSetOrange.png";

	private PackedScene PlayerScene = GD.Load<PackedScene>("res://Player/player.tscn");

	int numberOfPlayers = 0;

	List<Player> playersCreated;

	private void disableCollisionWithTheAlreadyCreatedPlayers(Player player){
		foreach(Player p in playersCreated){
			player.AddCollisionExceptionWith(p);
		}
	}

	private  Player createPlayer1(Vector2 initialPosition){
		Player player = PlayerScene.Instantiate() as Player;
		player.setInitialPosition(initialPosition);
		player.setControllerLayout(0);
		player.setSpriteReference(PURPLE_SPRITE_PATH);
		return player;
	}

	private  Player createPlayer2(Vector2 initialPosition){
		Player player = PlayerScene.Instantiate() as Player;
		player.setInitialPosition(initialPosition);
		player.setControllerLayout(1);
		player.setSpriteReference(ORANGE_SPRITE_PATH);
		return player;
	}

	public Player createPlayer(Vector2 initialPosition){
		numberOfPlayers++;
		Player player = null;
		if(numberOfPlayers == 1){
			player = createPlayer1(initialPosition);
		}else if(numberOfPlayers == 2){
			player = createPlayer2(initialPosition);
		}else{
			return null;
		}
		disableCollisionWithTheAlreadyCreatedPlayers(player);
		playersCreated.Add(player);
		return player;
	}

	public PlayerFactory(){
		playersCreated = new List<Player>();
	}
}
