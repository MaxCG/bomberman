/* 
Contém a implementação de uma bomba que além de explodir em cruz, explode também
nas diagonais.
*/

using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;

public class XExplosionBehavior : IExplosionBehavior {
	private int range;

	public XExplosionBehavior() {
		range = GlobalConfig.initialBombRange;
	}

	public XExplosionBehavior(int explosionRange) {
		range = explosionRange;
	}

	public Array<Vector2I> Expand(Func<Vector2I, Map.Tile> map, Vector2I center) {
		var exploded = new Array<Vector2I>();

		var directions = new List<Vector2I>{
			Vector2I.Up, Vector2I.Right, Vector2I.Down, Vector2I.Left,
			new Vector2I(1, 1), new Vector2I(1, -1), new Vector2I(-1, 1), new Vector2I(-1, -1)};

		foreach (var d in directions) {
			for (int i = 0; i <= range; i++) {
				var pos = center + i * d;
				if (map(pos) != Map.Tile.Wall) {
					exploded.Add(pos);
				} else {
					break;
				}
			}
		}

		return exploded;
	}

	public void IncreaseExplosionRange(int increment) {
		range += increment;
	}
	
	public string getSpritePath(){
		return GlobalConfig.GetBombPath(GlobalConfig.BombTypes.Cross);
	}

	public IExplosionBehavior clone() {
		return new XExplosionBehavior(range);
	}
}
