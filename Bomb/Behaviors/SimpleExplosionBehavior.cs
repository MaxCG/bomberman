/* 
Contém a implementação de uma explosão simples de uma bomba, em formato
de cruz.
*/

using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;

public class SimpleExplosionBehavior : IExplosionBehavior {
	private int range;

	public SimpleExplosionBehavior() {
		range = GlobalConfig.initialBombRange;
	}

	public SimpleExplosionBehavior(int explosionRange) {
		range = explosionRange;
	}

	public Array<Vector2I> Expand(Func<Vector2I, Map.Tile> map, Vector2I center) {
		var exploded = new Array<Vector2I>();

		var directions = new List<Vector2I>{Vector2I.Up, Vector2I.Right, Vector2I.Down, Vector2I.Left};

		foreach (var d in directions) {
			for (int i = 0; i <= range; i++) {
				var pos = center + i * d;
				if (Map.canPutExplosion(map(pos))) {
					exploded.Add(pos);
				} else {
					break;
				}
			}
		}

		return exploded;
	}

	public void IncreaseExplosionRange(int increment) {
		range += increment;
	}
	
	public string getSpritePath(){
		return GlobalConfig.GetBombPath(GlobalConfig.BombTypes.Standard);
	}

	public IExplosionBehavior clone() {
		return new SimpleExplosionBehavior(range);
	}
	
	
}
