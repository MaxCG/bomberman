/*
Interface para definir comportamentos de expansão da explosão de forma
personalizada.
*/

using System.Collections.Generic;
using Godot;
using System;
using Godot.Collections;

public interface IExplosionBehavior {
	Array<Vector2I> Expand(Func<Vector2I, Map.Tile> map, Vector2I center);

	void IncreaseExplosionRange(int increment);
	
	string getSpritePath();

	IExplosionBehavior clone();
}
