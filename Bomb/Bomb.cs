/*
Objeto 2D que representa a bomba no jogo. Contém propriedades como tempo
até explodir, sinal de ativação da explosão e métodos de explosão.
*/

using Godot;
using System.Collections.Generic;

public partial class Bomb : CharacterBody2D {
	[Export]
	private Timer explosionTimer;

	private IExplosionBehavior behavior;

	public Player bombOwner;

	[Signal]
	public delegate void ExplodedEventHandler(Bomb b);
	
	private bool existMoreThanOneProcessPhysics = false;
	
	private void setSpriteReference() {
		Sprite2D sprite = GetNode<Sprite2D>("Sprite2D");
		sprite.Texture = GD.Load<Texture2D>(behavior.getSpritePath());
	}
	
	public void setBehavior(IExplosionBehavior behavior_){
		behavior = behavior_;
		setSpriteReference();
	}
	
	public IExplosionBehavior getBehavior(){
		return behavior;
	}

	private void Explode() {
		EmitSignal(SignalName.Exploded, this);
		GetParent().RemoveChild(this);
		bombOwner.IncreaseAvailableBombs();
	}
	
	private void addCollisionExceptionWithPlayers(List<Player> players){
		foreach(Player player in players){
			AddCollisionExceptionWith(player);
		}
	}
	
	public void initiateBomb(List<Player> players){
		addCollisionExceptionWithPlayers(players);
	}
	
	private bool IsPlayerIsCollidingWithBomb(PhysicsBody2D player){
		if(!existMoreThanOneProcessPhysics){
			return true;
		}
		
		Area2D area = GetNode("Area2D") as Area2D;
		foreach (var bodyCollidingWithBomb in area.GetOverlappingBodies()) {
			if (bodyCollidingWithBomb.Name == player.Name) return true;
		}
		return false;
	}
	
	private void removeCollisionExceptionWithPlayerIfExitBomb(PhysicsBody2D player){
		if(!IsPlayerIsCollidingWithBomb(player)){
			 RemoveCollisionExceptionWith(player);
		}
	}
	
	private void removeCollisionExceptionWithPlayersWhenExitBomb(){
		
		foreach(PhysicsBody2D player in GetCollisionExceptions()){
			removeCollisionExceptionWithPlayerIfExitBomb(player);
		}
	}
	
	
	
	public override void _PhysicsProcess(double delta) {
		removeCollisionExceptionWithPlayersWhenExitBomb();
		existMoreThanOneProcessPhysics = true;
	}
}
