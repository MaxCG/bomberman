/*
Objeto 2D que representa um powerup na cena. É necessário inicializa-lo 
com uma estratégia de powerUpEffect para o correto funcionamento.
*/

using Godot;
using System;

public partial class PowerUp : Area2D
{
	public PowerUpEffect powerUpEffect;

	// Monitoring from Godot C# is broken
	private bool used = false;
	
	public void setSpriteReference() {
		Sprite2D sprite = GetNode<Sprite2D>("Sprite2D");
		sprite.Texture = GD.Load<Texture2D>(powerUpEffect.getSpritePath());
	}
	
	public void initiatePowerUp(PowerUpEffect effect) {
		powerUpEffect = effect;
		setSpriteReference();
	}
	
	private void OnBodyEntered(Node2D body) {
		if (body is Player && !used) {
			powerUpEffect.applyEffectOnPlayer(body as Player);
			GetParent().RemoveChild(this);
			used = true;
		}
	}
	
	public void setPosition(Vector2 initialPosition) {
		Position = initialPosition;
	}
}





