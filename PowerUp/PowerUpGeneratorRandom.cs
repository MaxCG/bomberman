/*
Classe abstrai a geração de powerup's randomicos para o jogo. 
Implementa apenas uma parte do algoritmo da criacao de powerup, referente
a escolha de qual efeito do powerup.
*/
using Godot;
using System;

public class PowerUpGeneratorRandom : PowerUpGenerator
{
	Random rng = new Random();

	protected override int generatePowerUpStrategyNumber(PowerUp powerup){
		return rng.Next((int) GlobalConfig.EffectTypes.NumberOfEffects);
	}
	
}
