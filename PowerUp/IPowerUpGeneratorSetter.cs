/*
Interface com a definição do que o HUB necessita saber para fazer a troca 
de geradores do objeto alvo.
*/

using Godot;
using System;

public interface IPowerUpGeneratorSetter
{
	void setPowerUpGenerator(PowerUpGenerator powerUpGenerator);
}
