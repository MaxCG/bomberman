/*
Classe abstrai a geração de powerup's para o jogo. 
Esta eh uma classe abstrata que utiliza o padrão de projeto TEMPLATE METHOD,
onde define a estrutura geral para criação de um PowerUp e a estrutura do 
mapeamento de inteiros para diferentes powerup's como definido na GlobalConfig,
com as suas subclasses definindo o algoritmo que escolhe qual powerup de fato
deve ser criado.
*/

using Godot;
using System;

public abstract class PowerUpGenerator
{
	private PackedScene PowerUpScene = GlobalConfig.GetPackedScene(GlobalConfig.Scenes.PowerUp);
	Random rng = new Random();
	
	protected void initiatePowerUpWithStrategy(PowerUp powerup, int strategyNumber){
		switch (strategyNumber) {
			case (int) GlobalConfig.EffectTypes.Fire:
				powerup.initiatePowerUp(new FireEffect());
				return;

			case (int) GlobalConfig.EffectTypes.Speed:
				powerup.initiatePowerUp(new SpeedEffect());
				return;

			case (int) GlobalConfig.EffectTypes.Heart:
				powerup.initiatePowerUp(new HeartEffect());
				return;
			case (int) GlobalConfig.EffectTypes.XBomb:
				powerup.initiatePowerUp(new XBombEffect());
				return;
			case (int) GlobalConfig.EffectTypes.ExtraBomb:
				powerup.initiatePowerUp(new ExtraBombEffect());
				return;
		}
	}
	
	protected abstract int generatePowerUpStrategyNumber(PowerUp powerup);
	
	public PowerUp generatePowerUp(Vector2 position) {
		PowerUp powerup = PowerUpScene.Instantiate() as PowerUp;
		powerup.setPosition(position);
		int powerupIndex = generatePowerUpStrategyNumber(powerup);
		initiatePowerUpWithStrategy(powerup,powerupIndex);
		return powerup;
	}
}
