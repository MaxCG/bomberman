/*
Classe abstrai a geração de powerup's para o jogo de forma sequencial de acordo
com a ordem definida nas configurações globais.
Implementa apenas uma parte do algoritmo da criacao de powerup, referente
a escolha de qual efeito do powerup.
*/
using Godot;
using System;

public class PowerUpGeneratorSequential : PowerUpGenerator
{
	private int num = -1;
	
	private void moveNumber(){
		num += 1;
		if(num == (int)GlobalConfig.EffectTypes.NumberOfEffects){
			num = 0;
		}
	}
	protected override int generatePowerUpStrategyNumber(PowerUp powerup){
		moveNumber();
		return num;
	}
	
}
