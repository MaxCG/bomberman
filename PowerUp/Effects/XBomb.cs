/*
Define o comportamento e o sprite do powerup de fazer a bomba em X.
*/

using Godot;
using System;

public class XBombEffect : PowerUpEffect {
	public override void applyEffectOnPlayer(Player player){
		player.setExplosionBehavior(new XExplosionBehavior());
	}
	
	public override string getSpritePath() {
		return GlobalConfig.GetSpritePath(GlobalConfig.EffectTypes.XBomb);
	}
}
