/*
Aumenta o número de bombas que um jogador pode colocar ao mesmo tempo.
*/

using Godot;
using System;

public class ExtraBombEffect : PowerUpEffect
{
	public override void applyEffectOnPlayer(Player player){
		player.IncreaseAvailableBombs();
	}
	
	public override string getSpritePath() {
		return GlobalConfig.GetSpritePath(GlobalConfig.EffectTypes.ExtraBomb);
	}
}
