/*
Define o comportamento e o sprite do powerup de aumentar o poder de fogo.
*/

using Godot;
using System;

public class FireEffect : PowerUpEffect
{
	public override void applyEffectOnPlayer(Player player){
		player.IncreaseExplosionRange(GlobalConfig.fireEffectIncrement);
	}
	
	public override string getSpritePath() {
		return GlobalConfig.GetSpritePath(GlobalConfig.EffectTypes.Fire);
	}
}
