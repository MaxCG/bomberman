/*
Define o comportamento e o sprite do powerup de aumentar a vida do jogador.
*/

using Godot;
using System;

public class HeartEffect : PowerUpEffect {
	public override void applyEffectOnPlayer(Player player){
		player.IncreaseHealth(GlobalConfig.heartEffectIncrement);
	}
	
	public override string getSpritePath() {
		return GlobalConfig.GetSpritePath(GlobalConfig.EffectTypes.Heart);
	}
}
