/*
Interface que define o contrato que um efeito de powerup deve respeitar
para ser considerado um.
*/

using Godot;
using System;

public abstract class PowerUpEffect
{
	public abstract void applyEffectOnPlayer(Player player);
	
	public abstract string getSpritePath();
}
