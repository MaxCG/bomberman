/*
Define o comportamento e o sprite do powerup de aumentar a velocidade
do jogador.
*/

using Godot;
using System;

public class SpeedEffect : PowerUpEffect
{
	public override void applyEffectOnPlayer(Player player){
		int speed = player.getSpeed() + GlobalConfig.speedEffectIncrement;
		if(speed > GlobalConfig.speedCap){
			speed = GlobalConfig.speedCap;
		}
		player.setSpeed(speed);
	}
	
	public override string getSpritePath() {
		return GlobalConfig.GetSpritePath(GlobalConfig.EffectTypes.Speed);
	}
}
