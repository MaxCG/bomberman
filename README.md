# Bomberman

## Como Executar

Esse jogo tem os seguintes requisitos:
    - [.NET v6.0.414](https://dotnet.microsoft.com/pt-br/download/dotnet/6.0)
    - [Godot v4.1.2](godotengine.org/download)

Após fazer o *download* das ferramentas acima, abra o Godot, vá na aba *Import*, selecione o arquivo `project.godot` que está na raíz do projeto e clique em `Accept and Edit`.

Por fim, selecione `Run Project` (o botão de *play* no canto superior direito).

## Integrantes

- Antonio Marcos Shiro Arnauts Hashisuca
- Eduardo Ribeiro Silva Oliveira
- João Henri Carrenho Rocha
- Luan Ícaro Pinto Arcanjo
- Luiz Carlos Costa da Silva
- Maximilian Cabrajac Göritz

## Descrição

Este é o projeto final para a disciplina MAC0413/5714 - Tópicos Avançados de Programação Orientada a Objetos. O objetivo deste trabalho é criar uma implementação moderna do famoso jogo Bomberman. Bomberman é um jogo multijogador, onde alguns jogadores são distribuídos por uma arena recheada de obstáculos destrutíveis com o objetivo de eliminar seus adversários. A forma principal de ataque contra os adversários e obstáculos é a utilização de bombas. Ao destruir obstáculos, existe a chance de aparecer uma melhoria (power-up) para as habilidades de um jogador. Um exemplo de melhoria simples seria o aumento do raio de explosão das bombas do jogador.

## Cronograma

### Fase 1

Primeira versão do jogo, implementação de um tutorial, onde o jogador é ensinado como colocar bombas para quebrar obstáculos e se movimentar no ambiente. Para isso, é necessário implementar:

- Colisão
- Movimentação do jogador
- Bombas
- Obstáculos

### Feedback Fase 1

Após o término da primeira versão do jogo, conseguimos implementar as seguintes funcionalidades:

- Criação do mapa por meio de um template
- Movimentação do jogador
- Implementação das bombas com seus efeitos visuais
- Implementação dos obstáculos
- Colisões entre o jogador, as bombas e o cenário

Objetivos não alcançados:

- Implementação de um tutorial intuitivo dos comandos, apesar de ser possível jogar em um mapa pré-criado

### Fase 2

Nesta fase será implementado o multiplayer local, o impacto das bombas no jogador e seu renascimento. Será iniciado também o trabalho em mais mecânicas descritas abaixo.

#### Power-ups

- Extensor: Aumenta o raio da explosão da bomba do jogador.
- Extensor Máximo: Aumenta o raio da explosão da bomba do jogador para o máximo possível.
- Acelerador: Aumenta a velocidade do jogador.
- Vida extra: Concede uma vida extra ao jogador.
- Bomba Extra: Inicialmente o jogador pode colocar uma bomba por vez, este consumível aumenta o limite.

#### Mapas com mecânicas especiais

- Mapa com teletransporte: No mapa do jogo terão portais interconectados que podem teletransportar jogadores, bombas e efeitos de explosão para outra localidade.
- Mapas com buracos: O mapa irá conter buracos no qual fazem os jogadores perderem uma vida ao pisarem.

## Feedback Fase 2

Após o término da segunda versão do jogo, conseguimos implementar as seguintes funcionalidades:

- Resolvido o bug colisão do player com a bomba pendente da primeira entrega.
- Criação de multiplayer local com 2 jogadores.
- Finalização e reinício do jogo no momento em que um dos jogadores perde todas as suas vidas.
- Criação de power-ups ao destruir obstáculos.
- Criação do power-up Extensor: Aumenta o raio da explosão da bomba do jogador.
- Criação do power-up Acelerador: Aumenta a velocidade do jogador.
- Criação do power-up Vida extra: Concede uma vida extra ao jogador.
- Adicionado comentários em cada Classe como solicitado no retorno da primeira entrega.
- Adicionado refatoração do PlayerState como solicitado no retorno da primeira entrega.
- Adicionado Menu de Comandos para mostrar teclas utilizadas para jogar.
- Adicionado Bomba ser colocada alinhada as posições do Mapa.

Os seguintes itens ficaram para a entrega 3:

- Implementar power-up Extensor Máximo.
- Implementar power-up Bomba Extra.
- Implementar Bombas com comportamentos diferentes.
- Implementar Mapas com mecânicas especiais.

Optamos por compor a entrega 3 exclusivamente por estes itens, abandonando a implementação do multiplayer online a princípio. Pois estes pontos já demandarão um tempo considerável de desenvolvimento e não conseguiremos a mais disto desenvolver um multiplayer online. Assim optando por abandona-lo por não ser uma característica principal do projeto.

### Fase 3

~~Por fim, serão implementado o multiplayer online.~~ Além disso, melhoraríamos a aparência do jogo.

Os seguintes itens serão implementados para a terceira e última entrega:

- Implementar os demais power-ups faltantes na segunda entrega. 
- Implementar Bombas com comportamentos diferentes.
- Implementar Mapas com mecânicas especiais.

## Feedback Fase 3

Após o término da terceira versão do jogo, conseguimos implementar as seguintes funcionalidades:

- Resolvido jogadores renascerem na origem, agora jogadores renascem no seu ponto original.
- Adicionado funcionalidade de escolha de diferentes mapas.
- Adicionado funcionalidade buracos ao mapa; No momento em que um jogador os toca, ele cairá e perderá uma vida.
- Criação de power-up Bomba Extra: inicialmente, o jogador pode colocar uma bomba por vez; Este consumível aumenta esse limite.
- Criação da bomba com comportamento de explosão em X.
- Criação de power-up Bomba X: a partir da coleta deste power-up, o jogador passa a colocar bombas com explosão em X em vez da padrão.
- Criação de Gerador de Power-up Sequencial: facilita testar manualmente o comportamento dos power-up em comparação ao Randômico.
- Adicionado funcionalidade para escolher diferentes tipos de geradores de power-ups.
- Criação de um HUB para o jogo, onde o jogadores podem visualizar o número de vidas restantes.
- Adicionado ao HUB a funcionalidade de escolher qual gerador de power-up quer utilizar em tempo de execução.
- Atualizado o sprite da bomba padrão.

Acabamos por não implementar na Fase 3:

- Power-up Extensor Máximo - Optamos por não implementá-lo por não adicionar uma funcionalidade interessante, é praticamente igual ao power-up Extensor original.
- Mecânica de Mapa Especial - Teletransporte.

### Padrões de Projeto Utilizados

#### Padrão State

É utilizado o padrão State para gerenciar os estados do jogador. Sem este padrão, a classe Player seria uma classe com muitas responsabilidades e com acoplamento muito alto, ocasionando código de péssima qualidade.

#### Padrão Strategy

É utilizado o padrão Strategy em duas ocasiões:

- Para definir os efeitos dos power-ups.
- Para definir os comportamentos(behavior) das bombas.

Este padrão é de grande utilidade nestes dois momentos. Como o power-up e a Bomb são classes parcias ligadas ao Godot e a uma cena, implementar herança nestas componentes adiciona uma alta complexidade ao projeto. Além disso, não sabemos se o Godot suporta herança de classes vinculadas a uma cena. Mas, nem precisamos buscar essa funcionalidade, uma vez que o padrão Strategy proporciona uma alternativa concisa.

#### Padrão Template

É utilizado o padrão Template na criação de diferentes geradores de power-up. Este caso é um exemplo claro da utilidade deste padrão, pois o processo de instanciar o power-up e atribuir seu efeito é genérico para todos os geradores. Assim, os geradores concretos são responsáveis apenas com a tarefa de implementar o algoritmo que escolhe qual efeito deve ser gerado.

#### Padrão Prototype

É utilizado o padrão Prototype no comportamento da bomba (bomb behavior). Ele é de grande utilidade, pois o comportamento é alterado a medida que o jogador coleta power-ups. Porém, as bombas que já estão colocadas devem manter seu comportamento original. Portanto, devemos fazer cópias dos comportamentos ao invés de existir apenas um, e é neste momento o padrão Prototype é útil.

### Sobre Testes Automatizados

Após uma intensa procura por um arcabouço de testes automatizados em Godot na linguagem C#, o único que conseguimos encontrar e que funcionou é o GDMut. 

O GDMut é um arcabouço muito limitado, a funcionalidade dele é apenas uma ferramenta de testes automatizados em C# com uma GUI integrada ao Godot, sem ter a capacidade de executar qualquer código que necessite da utilização do Godot. 

Dado a limitação do GDMut, o mesmo não suporta testar a vasta maioria do código criado neste projeto, com excessão de algumas funções muito pontuais que não utilizam qualquer funcionalidade do Godot. Essas funções podem ser encontradas na pasta Tests.
