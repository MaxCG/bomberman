/*
Arquivo do Godot para criar cenas ao clicar no botão.
*/

using Godot;

[Tool]
public partial class ChangeSceneButton : Button {
	[Export]
	private PackedScene scene;

	public override void _EnterTree() {
		this.Pressed += ChangeScene;
	}

	private void ChangeScene() {
		GetTree().ChangeSceneToPacked(scene);
	}
}
