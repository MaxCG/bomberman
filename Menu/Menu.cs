/*
Implementa a açao de sair do jogo.

Funciona em conjunto com a cena do Menu que define os botões de JOGAR
e SAIR.
*/

using Godot;
using System.IO;
using System.Collections.Generic;
using System;

public partial class Menu : CenterContainer {
	
	private void OnChangeMapItemSelected(int index){
		Map.ChangeMapTemplate(index-1);
	}
	
	public void OnQuitPressed() {
		GD.Print("Quiting game");
		GetTree().Quit();
	}
}
