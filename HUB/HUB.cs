/*
Classe responsável por criar os elementos do HUB, como por exemplo mostrar
a vida restantes dos jogadores.
*/

using Godot;
using System;

public partial class HUB : Control
{
	Player player1 = null;
	Player player2 = null;
	
	RichTextLabel labelPlayer1;
	RichTextLabel labelPlayer2;
	
	private IPowerUpGeneratorSetter powerupSetter;
	
	public void setPlayers(Player player1_, Player player2_){
		player1 = player1_;
		player2 = player2_;
	}
	
	public void setPowerUpSetter(IPowerUpGeneratorSetter setter){
		powerupSetter = setter;
	}
	
	private void setLabels(){
		labelPlayer1 = GetNode<RichTextLabel>("LabelPlayer1");
		labelPlayer2 = GetNode<RichTextLabel>("LabelPlayer2");
	}
	
	public override void _Ready()
	{
		setLabels();
	}
	
	private void setTextLabel(RichTextLabel label, int health, int number){
		string text = "[b] [center]  Player ";
		text = text + number.ToString() + ": ";
		text = text + health.ToString() + " Vida(s)";
		
		label.Text = text;
	}
	
	private void updateLabel(RichTextLabel label, Player player, int number){
		if(player != null){
			setTextLabel(label, player.getHealth(),number);
		}
	}
	
	private void updateLabelPlayer1(){
		updateLabel(labelPlayer1,player1,1);
	}
	
	private void updateLabelPlayer2(){
		updateLabel(labelPlayer2,player2,2);
	}
	
	public void OnChoosePowerUpGeneratorList(long index)
	{
		switch (index) {
			case 0:
				powerupSetter.setPowerUpGenerator(new PowerUpGeneratorRandom());
				break;
			case 1:
				powerupSetter.setPowerUpGenerator(new PowerUpGeneratorSequential());
				break;
		}
	}
	
	public override void _PhysicsProcess(double delta)
	{
		updateLabelPlayer1();
		updateLabelPlayer2();
	}

	public override void _Process(double delta)
	{
		updateLabelPlayer1();
		updateLabelPlayer2();
	}
}
