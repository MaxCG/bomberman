/*
Armazena configurações do jogo para centralizar o gerenciamento das mesmas.
*/

using Godot;
using System.Collections.Generic;

public class GlobalConfig {
	static private string prefix = "res://";

	static public float gameOverWaitTime = 1.0f;
	static public float playerRespawnTime = 0.5f;

	// Player initial stats
	static public int initialHealth = 3;
	static public int initialSpeed = 200;
	static public int initialBombRange = 3;
	static public float initialSpriteScale = 1.6f;

	static public int initialAvailableBombs = 1;

	static public float explosionDuration = 1.0f;

	// Powerups default effects
	static public int fireEffectIncrement = 1;
	static public int speedEffectIncrement = 20;
	static public int heartEffectIncrement = 1;
	

	static public int speedCap = 400;

	public enum EffectTypes {
		Fire,
		Speed,
		Heart,
		XBomb,
		ExtraBomb,
		NumberOfEffects
	}

	// Sprites paths
	static private string spritePath = "PowerUp/Sprites/";
	
	static public string GetSpritePath(EffectTypes effect) {
		var pngName = new Dictionary<EffectTypes, string>{
			{EffectTypes.Fire, "firepowerup.png"},
			{EffectTypes.Speed, "speedpowerup.png"},
			{EffectTypes.Heart, "heartpowerup.png"},
			{EffectTypes.XBomb, "xbombpowerup.png"},
			{EffectTypes.ExtraBomb, "extrabombpowerup.png"},
		};
		return prefix + spritePath + pngName[effect];
	}
	
	public enum BombTypes{
		Standard,
		Cross,
	}
	
	static private string bombSpritePath = "Bomb/Sprite/";
	
	static public string GetBombPath(BombTypes type){
		var pngName = new Dictionary<BombTypes,string>{
			{BombTypes.Standard, "bomb.png"},
			{BombTypes.Cross, "bomb_x.png"},
		};
		return prefix + bombSpritePath + pngName[type];
	}

	// Scenes paths
	public enum Scenes {
		Menu,
		Bomb,
		PowerUp,
	}

	static public PackedScene GetPackedScene(Scenes scene) {
		var scenes = new Dictionary<Scenes, string>{
			{Scenes.Menu, "Menu/MenuScene.tscn"},
			{Scenes.Bomb, "Bomb/Bomb.tscn"},
			{Scenes.PowerUp, "PowerUp/PowerUp.tscn"},
		};

		return ResourceLoader.Load<PackedScene>(prefix + scenes[scene]);
	}

	// Map Templates
	static string[] mapStrings = {
		"map1",
		"map2",
		"map3"
	};

	static public string GetMapTemplate(int index) {
		return prefix + "Map/Templates/" + mapStrings[index] + ".txt";
	}
}
