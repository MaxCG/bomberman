using GdMUT;

public class PlayerTest{
	#if TOOLS
	[CSTestFunction]
	public static Result testHealthIsGlobalConfig(){
		Player player = new Player();

		for(int i = 0; i < GlobalConfig.initialHealth; i++){
			player.ReduceHealth();
		}
		return player.HasHealth() ? Result.Failure : Result.Success;
	}

	#endif
}
