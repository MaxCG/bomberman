public class ToyPowerUpGeneratorSetter : IPowerUpGeneratorSetter{
    public PowerUpGenerator generator;

    public void setPowerUpGenerator(PowerUpGenerator gen){
        generator = gen;
    }
}