using GdMUT;

public class HUBTest{
	#if TOOLS
	[CSTestFunction]
	public static Result testChoosePowerUpGeneratorListInput0(){
		HUB hub = new HUB();
        ToyPowerUpGeneratorSetter toyGen = new ToyPowerUpGeneratorSetter();

        hub.setPowerUpSetter(toyGen);
        hub.OnChoosePowerUpGeneratorList(0);

		return toyGen.generator is PowerUpGeneratorRandom ? Result.Success : Result.Failure;
	}

    [CSTestFunction]
	public static Result testChoosePowerUpGeneratorListInput1(){
		HUB hub = new HUB();
        ToyPowerUpGeneratorSetter toyGen = new ToyPowerUpGeneratorSetter();

        hub.setPowerUpSetter(toyGen);
        hub.OnChoosePowerUpGeneratorList(1);

		return toyGen.generator is PowerUpGeneratorSequential ? Result.Success : Result.Failure;
	}

	#endif
}
