using GdMUT;
public class PowerUpEffectTest{
    #if TOOLS
	[CSTestFunction]
    public static Result testExtraBombEffectSpritePath(){
        PowerUpEffect effect = new ExtraBombEffect();
        string expected = "res://PowerUp/Sprites/extrabombpowerup.png";
        return expected == effect.getSpritePath() ? Result.Success : Result.Failure;
    }

    [CSTestFunction]
    public static Result testExtraBombApplyEffect(){
        Player player = new Player();
        PowerUpEffect effect = new ExtraBombEffect();
        effect.applyEffectOnPlayer(player);
        return Result.Success;
    }

    [CSTestFunction]
    public static Result testFireEffectApplyEffect(){
        Player player = new Player();
        PowerUpEffect effect = new FireEffect();
        effect.applyEffectOnPlayer(player);
        return Result.Success;
    }   

    [CSTestFunction]
    public static Result testHeartEffectApplyEffect(){
        Player player = new Player();
        PowerUpEffect effect = new HeartEffect();
        effect.applyEffectOnPlayer(player);

        return player.getHealth() == GlobalConfig.initialHealth+1 ? Result.Success : Result.Failure;
    }   

    [CSTestFunction]
    public static Result testSpeedEffectApplyEffect(){
        Player player = new Player();
        PowerUpEffect effect = new SpeedEffect();
        effect.applyEffectOnPlayer(player);
        bool result = player.getSpeed() == GlobalConfig.initialSpeed+GlobalConfig.speedEffectIncrement;
        return result ? Result.Success : Result.Failure;
    } 

    [CSTestFunction]
    public static Result testSpeedEffectApplyEffectIfCapped(){
        Player player = new Player();
        player.setSpeed(GlobalConfig.speedCap);
        PowerUpEffect effect = new SpeedEffect();
        effect.applyEffectOnPlayer(player);
        bool result = player.getSpeed() == GlobalConfig.speedCap;
        return result ? Result.Success : Result.Failure;
    } 

    [CSTestFunction]
    public static Result testXbombEffectApplyEffect(){
        Player player = new Player();
        PowerUpEffect effect = new XBombEffect();
        effect.applyEffectOnPlayer(player);
        return Result.Success;
    }

    #endif
}